import java.util.*;
public class Main {
    public static void main(String[] args) {
        List<Figure> figuri = new ArrayList<>();
        figuri.add(new Rectangle("Прямоугольник",7.5,2, 0, 0));
        figuri.add(new Circle("Круг",0,0, 8, 0, 0));
        figuri.add(new Triangle("Треугольник",5, 5.6, 3, 0, 0));
        for (Figure a:figuri) {
            System.out.printf(a.toString());
        }
    }
}